<?php

class OnThisDaySettings {

	/**
	 * Singleton design pattern implementation
	 */

	protected static $instance = null;


	/**
	 * Create instance of this class if none exists.
     * If it exists, just return it.
	 *
	 * @return self
	 */
	public static function instance() {
		if (static::$instance === null ) {
			static::$instance = new self();
		}
		return static::$instance;
	}
	

	/**
	 * When uninstalling the plugin, remove the option from database
	 */
	public static function uninstall() {
		delete_option( 'onthisday_default_image' );
	}


	/**
	 * Register the setting on the media settings page.
	 */
	public function add_media_section() {

        add_settings_section(
            'onthisday', // ID
            'Settings for OnThisDay plugin', // Section Title
            array(&$this, 'onthisday_section_header'), // Callback
            'media' // which page
        );

		register_setting(
			'media', // which page
			'onthisday_default_image', // option name
			array( &$this, '' ) // validation callback
		);

		add_settings_field(
			'onthisday', // ID
			'Default image', // Setting title
			array( &$this, 'onthisday_section_content' ), // Callback
			'media', // which page
			'onthisday' // which section id
		);
	}

    public function onthisday_section_header( $arg ) {
        echo "<div style='font-size: 12px;'>
                The image that is selected here will be added in the RSS feed 
                generated with the OnThisDay plugin for the posts that don't have 
                featured image. It won't affect any other portion of the site.
                <br />
                If you no longer want to have default image, use the button 
                to unset the image that is currently set.
                <br />
                Don't forget to hit 'Save changes' when you are done.
            </div>";       
      
    }

	/**
	 * Display the buttons and a preview on the media settings page.
	 */
	public function onthisday_section_content() {
		$value = get_option( 'onthisday_default_image' );
        ?>

        <div style="display: flex; align-items: center;">

            <div id="onthisday-image-preview" style="float:left; padding: 0 5px 0 0;">
                <?php echo $this->preview_image( $value ); ?>
            </div>
            
            <input type="hidden" id="onthisday_image_id" value="<?php echo esc_attr( $value ); ?>" name="onthisday_default_image"/>

            <a id="onthisday-set" class="button" href="#" style="margin: 0 5px">
                <span style="margin-top: 4px;"  class="dashicons dashicons-format-image">
                </span>
                Select default image
            </a>
            
            <a id="onthisday_unset"  style="margin: 0 5px" class="button <?php echo empty($value) ? 'button-disabled' : '' ?>" href="#">
                <span style="margin-top: 4px;"  class="dashicons dashicons-no"></span>
                Unset current image
            </a>

        </div>
		
        
		<?php
	}

	/**
	 * Checks if the given input is a valid image.
	 *
	 * @param string|int $thumbnail_id
	 *
	 * @return string|bool
	 */
	public function input_validation($thumbnail_id) {
		if ( wp_attachment_is_image($thumbnail_id)) {
			return $thumbnail_id;
		}

		return false;
	}

	/**
	 * Register the javascript file
	 */
	public function admin_scripts() {
		wp_enqueue_media(); //The media popup
		wp_enqueue_script( 'onthisday', plugin_dir_url(__FILE__) . 'onthisday-settings.js');
	}

	/**
	 * Return an image tag that will be show as preview
	 *
	 * @param int $image_id A valid attachment image ID.
	 *
	 * @return string
	 */
	public function preview_image($image_id) {
        if(empty($image_id)) {
            return '';
        }
		return wp_get_attachment_image($image_id, array( 80, 60 ), true);
	}

	/**
	 * The callback for the ajax call when the admin changes the default image
	 */
	public function ajax_preview_image() {
		if (!empty( $_POST['image_id'] ) && absint($_POST['image_id'])) {
			$img_id = absint($_POST['image_id']);
			echo $this->preview_image($img_id);
		}
		die();
	}

	/**
	 * Add a settings button next to the plugin on the plugin page
	 */
	public function add_settings_link($links) {
		$href          = admin_url('options-media.php#onthisday');
		$settings_link = "<a href='{$href}'>Settings</a>";
		array_unshift($links, $settings_link);

		return $links;
	}
}
