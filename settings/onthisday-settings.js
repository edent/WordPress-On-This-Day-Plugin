jQuery(document).ready(function ($) {
	'use strict';   

	// Set the content of the preview image box
	function set_preview_html(html) {
		$("#onthisday-image-preview").html(html);
		$('#onthisday_unset').removeClass('button-disabled');
	}

	// Show loading image until the ajax call complets
	function set_loading_image() {
		$("#onthisday-image-preview").html("<img src='images/loading.gif'/>");
	}

	// On closing media popup, send the image to backend to generate a preview and show it
	function save_default_onthisday_image(selected_image_id) {
		$('#onthisday_image_id').val(selected_image_id);
		
		set_loading_image();

		var data = {
            action: 'onthisday_preview_image',
            image_id: selected_image_id
        };

		$.post(ajaxurl, data, function (response) {
			set_preview_html(response);
		});
	}

	// Remove default image
	$('#onthisday_unset').click(function (e) {
		e.preventDefault();
		$('#onthisday-image-preview').html("");
		$('#onthisday_image_id').val('');
		$(this).addClass('button-disabled');
	});

	// Open the media manager to select an image
	 $('#onthisday-set').click(function (e) {
		e.preventDefault();
		var frame = wp.media({
			title : 'Select default image for the onthisday plugin',
			multiple : false,
			library : { type : 'image' },
			button : { text : 'Set default image' }
		});

		// Event on closing media manager: get selected image and set is as a default image
		frame.on('close', function () {
			var images = frame.state().get('selection');
	
			images.each(function (image) {
				save_default_onthisday_image(image.id);
			});
		});

		// Event on open media manager: preselect the currently setup image
		frame.on('open', function () {
			var attachment,
				selection = frame.state().get('selection'),
				id = $('#onthisday_image_id').val();

			attachment = wp.media.attachment(id);
			attachment.fetch();

			selection.add(attachment ? [ attachment ] : []);
		});

		// Finally, open the media manager
		frame.open();
	});
});
